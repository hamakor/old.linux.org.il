<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
	<xsl:param name="year"/>
	<xsl:param name="month"/>
	<xsl:param name="day"/>

	<xsl:include href="events_base.xsl"/>

	<xsl:template match="/events">
		<!-- Match only events for this and the next months -->
		<xsl:variable name="events" select="event[
	(date/year/text()=$year and date/month/text()=$month)
	or
	(date/year/text()=$year and date/month/text()=$month+1)
	or
	(date/year/text()=$year+1 and $month=12 and date/month/text()=1)
]"/>

		<!-- Avoid adding empty UL; HTML strictness -->
		<xsl:choose>
			<xsl:when test="$events">
				<dl class="events-list">
					<xsl:apply-templates select="$events"/>
				</dl>
			</xsl:when>
			<xsl:otherwise>
				<p>
				לא פורסמו באתר ארועים לחודשיים הקרובים.
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="event">
		<dt>
			<xsl:apply-templates select="@type" mode="symbol"/>
			[<xsl:apply-templates select="date"/>]
			<xsl:apply-templates select="@type" mode="name"/>
			:
			<xsl:choose>
				<xsl:when test="url">
					<a href="{url/text()}">
						<xsl:apply-templates select="name"/>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="name"/>
				</xsl:otherwise>
			</xsl:choose>
		</dt>
		
		<dd>
			<dl>
				<xsl:if test="@organizer">
					<dt>מארגן:</dt>
					<dd><xsl:apply-templates select="@organizer"/></dd>
				</xsl:if>
				<xsl:if test="@location">
					<dt>מיקום:</dt>
					<dd><xsl:apply-templates select="@location"/></dd>	
				</xsl:if>
				<xsl:if test="synopsis">
					<dt>תקציר:</dt>
					<dd><xsl:apply-templates select="synopsis"/></dd>	
				</xsl:if>
			</dl>
		</dd>
	</xsl:template>
</xsl:stylesheet>
