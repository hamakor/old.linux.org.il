<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!-- Create keys for faster lookups -->
	<xsl:key name="types" match="/events/type" use="@id"/>

	<xsl:key name="organizers" match="/events/organizer" use="@id"/>

	<xsl:key name="locations" match="/events/location" use="@id"/>

	<xsl:template match="event/@type" mode="symbol">
		<img src="{key('types', .)/symbol/@src}" class="symbol"/>
	</xsl:template>

	<xsl:template match="event/@type" mode="name">
		<xsl:value-of select="key('types', .)/name/text()"/>
	</xsl:template>

	<xsl:template match="event/date">
		<xsl:value-of select="year/text()"/><xsl:if test="month/text()">-<xsl:value-of select="month/text()"/>	<xsl:if test="day/text()">-<xsl:value-of select="day/text()"/></xsl:if></xsl:if>
	</xsl:template>

	<xsl:template match="event/name">
		<span class="event-title"><xsl:value-of select="text()"/></span>
	</xsl:template>
	
	<xsl:template match="event/synopsis">
		<xsl:apply-templates select="text()|br"/>
	</xsl:template>
	
	<xsl:template match="br">
		<xsl:element name="br"/>
	</xsl:template>

	<xsl:template match="event/@organizer">
		<xsl:variable name="organizer" select="key('organizers',.)"/>
		<xsl:choose>
			<xsl:when test="$organizer/url">
				<a href="{$organizer/url/text()}" class="event_organizer"><xsl:value-of select="$organizer/name/text()"/></a>
			</xsl:when>
			<xsl:otherwise>
				<span class="event_organizer"><xsl:value-of select="$organizer/name/text()"/></span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="event/@location">
		<xsl:variable name="location" select="key('locations',.)"/>
		<xsl:choose>
			<xsl:when test="$location/url">
				<a href="{$location/url/text()}" class="event_location"><xsl:value-of select="$location/name/text()"/></a>
			</xsl:when>
			<xsl:otherwise>
				<span class="event_location"><xsl:value-of select="$location/name/text()"/></span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
