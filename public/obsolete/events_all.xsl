<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>

	<xsl:include href="events_base.xsl"/>

	<xsl:template match="/events">
		<dl class="events-list">
			<!-- Match all events -->
			<xsl:apply-templates select="event">
				<xsl:sort select="date/year/text()" data-type="number" order="descending"/>
				<xsl:sort select="date/month/text()" data-type="number" order="descending"/>
				<xsl:sort select="date/day/text()" data-type="number" order="descending"/>
			</xsl:apply-templates>
		</dl>
	</xsl:template>
	
	<!-- Symbols -->
	
	<xsl:template match="event">
		<dt>
			<xsl:apply-templates select="@type" mode="symbol"/>
			[<xsl:apply-templates select="date"/>]
			<xsl:apply-templates select="@type" mode="name"/>
			:
			<xsl:choose>
				<xsl:when test="url">
					<a href="{url/text()}">
						<xsl:apply-templates select="name"/>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="name"/>
				</xsl:otherwise>
			</xsl:choose>
		</dt>
		
		<dd>
			<dl>
				<xsl:if test="@organizer">
					<dt>מארגן:</dt>
					<dd><xsl:apply-templates select="@organizer"/></dd>
				</xsl:if>
				<xsl:if test="@location">
					<dt>מיקום:</dt>
					<dd><xsl:apply-templates select="@location"/></dd>	
				</xsl:if>
				<xsl:if test="synopsis">
					<dt>תקציר:</dt>
					<dd><xsl:apply-templates select="synopsis"/></dd>
				</xsl:if>
			</dl>
		</dd>
	</xsl:template>
</xsl:stylesheet>
