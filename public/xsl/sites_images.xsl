<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
	
	<xsl:param name="sites_per_row" select="3"/>

	<xsl:template match="/sites">
		<xsl:variable name="image_sites" select="descendant::category/site[image and not(@disabled)]"/>
		<table class="sites-table">
			<!-- Match the first item of every row; note that position() is 1-based -->
			<xsl:for-each select="$image_sites[((position()-1) mod $sites_per_row) = 0]">
				<xsl:variable name="start_position" select="((position()-1) * $sites_per_row) + 1"/>
				<tr>
					<!-- Match the items of the row ($start_position .. $start_position+$sites_per_row-1) -->
					<xsl:apply-templates select="$image_sites[(position()&gt;=$start_position) and (position()&lt;$start_position+$sites_per_row)]" mode="image"/>
				</tr>
			</xsl:for-each>
		</table>
		
		<div class="more-links">
			<xsl:apply-templates select="category" mode="name"/>
		</div>
	</xsl:template>
	
	<xsl:template match="category" mode="name">
		<a href="/sites/{@id}"><xsl:value-of select="name"/></a>
		<xsl:if test="position()!=last()">
			|
		</xsl:if>
	</xsl:template>

	<xsl:template match="site[image]" mode="image">
		<td style="vertical-align:top" class="site-logo-cell">
			<a href="{url}"><img src="site_images/{@id}" alt="{name}" class="site-logo"/></a>
			<div class="site-log-description-block">
				<xsl:value-of select="blurb"/>
			</div>
		</td>
	</xsl:template>
</xsl:stylesheet>
