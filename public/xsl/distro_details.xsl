<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>

	<xsl:param name="distro"/>
	
	<xsl:key name="archs" match="/distros/arch" use="@id"/>

	<xsl:template match="/distros">
		<xsl:variable name="distro_element" select="distro[@id=$distro]"/>
		<xsl:choose>
			<xsl:when test="$distro_element">
				<xsl:apply-templates select="$distro_element"/>
			</xsl:when>
			<xsl:otherwise>
				אין הפצה בשם הזה.
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="distro">
		<h2>
			<xsl:value-of select="name/text()"/>
		</h2>

		<p>
			<xsl:value-of select="description/text()"/>
		</p>

		<p class="more-links">
מידע נוסף:

<a><xsl:attribute name="href"><xsl:value-of select="url/text()"/></xsl:attribute>
אתר הבית</a>
		</p>

		<xsl:choose>
			<xsl:when test="dlversion">
				<a name="download"/>
				<h1>הורדה בחינם</h1>
				<!-- If we have any ISO links, add an explanation about ISO files. We display it only once for the entire list of versions. -->
				<xsl:if test="dlversion/dllink[@type='iso']">
					<div class="help-box">
קובץ ISO הוא קובץ המכיל העתק מדוייק של תקליטור. שמירת הדיסק כקובץ בודד מאפשרת להוריד תקליטור שלם במרוכז ולצרוב עותקים של התקליטור בקלות, בכל כונן צורב CD-R. גודלו של הקובץ הוא הגודל הכולל של התקליטור  &#x2013; בדרך-כלל כ 650 מגבייט. הרבה הפצות לינוקס מכילות יותר מתקליטור אחד וכל תקליטור הוא קובץ ISO נפרד שיש להוריד ולצרוב.
					</div>
				</xsl:if>				
				<xsl:apply-templates select="dlversion"/>
			</xsl:when>
			<xsl:otherwise>
אין מידע זמין לגבי הורדת ההפצה הזאת. ניתן לבקר באתר הבית של ההפצה לקבלת פרטים נוספים.
			</xsl:otherwise>
		</xsl:choose>
ניתן למצוא מקומות נוספים בהם ניתן להוריד הפצה זו ואחרות ב<a href="http://mirror.hamakor.org.il/">אתר המראה של עמותת המקור</a>.
		
		<xsl:if test="dllink[@type='dir']">
			<h1>
				הורדת קבצים וספריות
			</h1>

<table class="links-table">
	<tr>
		<th>שם</th>
		<th>קישור</th>
	</tr>
	<xsl:for-each select="dllink[@type='dir']">
		<tr>
			<td><xsl:value-of select="name/text()"/></td>
			<td>
				<a>
				<xsl:attribute name="href"><xsl:value-of select="url/text()"/></xsl:attribute>
				<xsl:value-of select="url/text()"/>
				</a>
			</td>
		</tr>
	</xsl:for-each>
</table>

		</xsl:if>
	</xsl:template>
	
	<xsl:template match="dlversion">
		<h2>
גרסא
<xsl:value-of select="@version"/>
למחשבי
		<xsl:value-of select="key('archs',@arch)/name/text()"/>
		&#x200f;
		(<xsl:value-of select="key('archs',@arch)/description/text()"/>)
		</h2>
		<xsl:if test="dllink[@type='iso']">
			<p>
הקישורים הבאים מובילים לקבצי דיסקים (מסוג ISO) הניתנים לצריבה:
			</p>
			<ul>
				<xsl:apply-templates select="dllink[@type='iso']"/>
			</ul>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="dllink[@type='iso']">
		<li>
			<xsl:variable name="first_url" select="url[position() = 1]"/>
			<xsl:variable name="other_urls" select="url[position() > 1]"/>

			<a>
				<xsl:attribute name="href"><xsl:value-of select="$first_url/text()"/></xsl:attribute>
				<xsl:value-of select="name/text()"/>
			</a>

			<xsl:if test="$other_urls">
				(
אתרי הורדה אלטרנטיביים:
				<xsl:for-each select="$other_urls">
					<a>
						<xsl:attribute name="href"><xsl:value-of select="text()"/></xsl:attribute>
						<xsl:value-of select="position()"/>
					</a>
				</xsl:for-each>
				)
			</xsl:if>
		</li>
	</xsl:template>	
</xsl:stylesheet>
