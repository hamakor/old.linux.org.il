<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>

	<xsl:template match="/distros">
		<xsl:apply-templates select="distro"/>
	</xsl:template>

	<xsl:template match="distro">
		<li>
			<h2>
				<a href="{@id}/">
					<xsl:value-of select="name/text()"/>
				</a>
			</h2>

			<p>
				<xsl:value-of select="description/text()"/>
			</p>

			<p class="more-links">
מידע נוסף:

<a><xsl:attribute name="href"><xsl:value-of select="url/text()"/></xsl:attribute>
אתר הבית</a>

				<xsl:if test="dlversion">
|
<a><xsl:attribute name="href"><xsl:value-of select="@id"/>/#download</xsl:attribute>הורדה</a>
				</xsl:if>


			</p>
		</li>
	</xsl:template>

</xsl:stylesheet>
