<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>

	<xsl:template match="/sites">
		<xsl:apply-templates select="category"/>
	</xsl:template>

	<xsl:template match="category">
		<div class="item"><a href="/sites/{@id}/"><xsl:value-of select="name/text()"/></a>&#160;<img src="/menu_bullet.png" alt="&#x2022;"/></div>
	</xsl:template>
</xsl:stylesheet>
