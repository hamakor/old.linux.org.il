<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>

	<xsl:param name="category_id" select="'*'"/>

	<xsl:template match="/sites">
		<xsl:choose>
			<xsl:when test="$category_id = '*'">
				<ul>
					<xsl:apply-templates select="category" mode="index"/>
				</ul>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="category[@id=$category_id]" mode="display"/>
				<xsl:if test="$category_id = 'training' or $category_id = 'business'">
					<p style="font-size:smaller;">
					* תיאורי החברות הינם מאת החברות עצמן ובאחריותן בלבד.
					</p>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Format top-level categories differently -->
	<xsl:template match="/sites/category" mode="display">
		<h1><xsl:value-of select="name/text()"/></h1>
		<xsl:apply-templates select="category" mode="display"/>
	</xsl:template>

	<xsl:template match="category" mode="display">
		<a name="{@id}"/>
		<h2><xsl:value-of select="name/text()"/></h2>
		<div style="margin-right:20px">
			<xsl:apply-templates select="category"/>
			<dl class="sites-list">
				<xsl:apply-templates select="site[not(@disabled)]"/>
			</dl>
		</div>
	</xsl:template>

	<xsl:template match="category" mode="index">
		<li>
			<a name="{@id}"/>
			<a href="{@id}/">
				<xsl:value-of select="name/text()"/>
			</a>
		</li>
	</xsl:template>

	<xsl:template match="site">
		<dt>
			<xsl:choose>
				<xsl:when test="@language = 'en'">
					<img src="/icon_lang_en.png" alt="סמל אנגלית" class="symbol"/>
				</xsl:when>
				<xsl:when test="@language = 'he'">
					<img src="/icon_lang_he.png" alt="סמל עברית" class="symbol"/>
				</xsl:when>
			</xsl:choose>

			<!-- Prevent the icon from blending with LTR text -->
			<xsl:text disable-output-escaping="yes">
			&amp;lrm;&amp;rlm;
			</xsl:text>

			<a href="{url}">
				<xsl:if test="disabled">
					<xsl:attribute name="style">text-decoration:line-through</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="name"/>
			</a>
		
			<!-- Prevent all-English strings from breaking -->
			<xsl:text disable-output-escaping="yes">
			&amp;lrm;&amp;rlm;
			</xsl:text>

	</dt>

		<dd>
			<xsl:value-of select="description"/>
			<xsl:value-of select="disabled"/>
		</dd>
		
	</xsl:template>
</xsl:stylesheet>
