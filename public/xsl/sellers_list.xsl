<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>

	<xsl:template match="/distros">
		<xsl:apply-templates select="seller"/>
	</xsl:template>

	<xsl:template match="seller">
	<dt>
		<a>
			<xsl:attribute name="href"><xsl:value-of select="url/text()"/></xsl:attribute>
			<xsl:value-of select="name/text()"/>
		</a>
	</dt>
	<dd>
		<xsl:value-of select="description/text()"/>
	</dd>
	</xsl:template>

</xsl:stylesheet>